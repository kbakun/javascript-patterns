/* 
 * przepis na wołanie łańcuchowe sprowadza się do zwrócenia na końcu funkcji naszego obiektu. 
 * zmienna `that` pozwala zagwarantować, że zwracamy referencję do obiektu głownego
 * 
 */


var Sql = function Sql() {
        'use strict';
        var that = this,
            data = {
                type: "select",
                columns: ["*"],
                table: "table",
                where: "",
                conditions: [],
                not: false,
                firstWhere: true
            };
        this.select = function () {
            var i = 0;
            data.type = "select";
            if (arguments.length > 0) {

                data.columns = [];
                for (i = 0; i < arguments.length; i += 1) {
                    data.columns.push(arguments[i]);
                }
            }
            data.columns = ["*"];
            return that;
        };
        this.update = function (table) {
            data.type = "update";
            data.table = table;
            data.columns = [];
            return that;
        };
        this.del = function () {
            data.type = "delete";
            data.columns = [];
            return that;
        };
        this.from = function (table) {
            data.table = table;
            return that;
        };
        this.columns = function (columns) {
            data.columns = columns;
            return that;
        };
        this.set = function (column, value) {
            data.columns.push(column + "='" + value + "'");
            return that;
        };
        this.and = function () {
            data.conditions.push("and");
            return that;
        };
        this.or = function () {
            data.conditions.push("or");
            return that;
        };
        this.where = function (column) {
            if (data.firstWhere === true) {
                data.conditions.push("where");
                data.firstWhere = false;
            }
            data.conditions.push(column);
            return that;

        };
        this.eq = function (value) {
            if (data.not) {
                data.conditions.push("<>");
            } else {
                data.conditions.push("=");
            }
            data.not = false;
            data.conditions.push(value);
            return that;
        };
        this.not = function () {
            data.not = true;
            return that;
        };
        this.toString = function () {
            if (data.type === "select") {
                return "SELECT " + data.columns.join(",") + " FROM " + data.table + " " + data.where + data.conditions.join(" ");
            }
            if (data.type === "update") {
                return "UPDATE " + data.table + " SET " + data.columns.join(",") + " " + data.where + data.conditions.join(" ");
            }
            if (data.type === "delete") {
                return "DELETE FROM " + data.table + " " + data.where + data.conditions.join(" ");
            }
            throw "bad type";
        };
    };
    
    
var select = new Sql().select("*").from("test").where("id").eq(123).toString();
var update = new Sql().update("test").set("imie", "a").where("id").eq(123).toString();
var del = new Sql().del().from("test").where("id").eq(123).toString();