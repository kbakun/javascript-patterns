/*
 * przepis na funkcję z weryfikacją argumentów - oczywiście sposób weryfikacji argumentów zależy od potrzeb
 * można weryfikować zarówno czy zostały podane jak i ich typ i ewentualnie założenia graniczne
 * np. długość stringu, zakres dat, zakres wartości
 * 
 */

function imieNazwisko() {
    'use strict';
    var i = 0,
        ret = "";

    if (arguments.length < 2) {
        throw {
            nr: 1,
            desc: "za ma\u0142o argumentów"
        };
    }
    for (i = 0; i < arguments.length; i += 1) {
        if (typeof arguments[i] !== 'string') {
            throw {
                nr: 2,
                desc: "agument nr " + (i + 1) + " musi by\u0107 stringiem"
            };
        }
        ret += " " + arguments[i];
    }
    return ret;

}
//    imieNazwisko(""); -- wywoła błąd
//    imieNazwisko(); -- wywoła błąd
//    imieNazwisko("","",null); -- wywoła błąd
//    imieNazwisko("","",undefined); -- wywoła błąd
//    imieNazwisko("","",function(){});  -- wywoła błąd
//    imieNazwisko("",""); zwróci string
