/*
 * przepis na wzorzec obserwator
 * obiekt obserwowany udostępnia 3 metody 
 * 'obserwuj' - inicjuje obserwację,
 * 'nieobserwuj'- przerywa obserwację,
 * 'publikuj' - powiadamia aktualnie obserwujących
 * 
 */

var Obserwowany = function Obserwowany() {
        'use strict';
        var self = this;
        self.obserwatorzy = [];
        self.obserwuj = function (kto) {
            self.obserwatorzy.push(kto);
            console.log("start obserwacji");
        };
        self.nieobserwuj = function (kto) {
            var i;
            for (i = 0; i < self.obserwatorzy.length; i += 1) {
                if (this.obserwatorzy[i] === kto) {
                    self.obserwatorzy.splice(i, 1);
                    console.log("stop obserwacji");
                    return;
                }
            }

        };
        self.publikuj = function (dane) {
            var i;
            for (i = 0; i < self.obserwatorzy.length; i += 1) {
                self.obserwatorzy[i](dane);

            }
        };


    };
var Obserwator = function Obserwator(nazwa) {
        'use strict';
        var self = this;
        self.nazwa = nazwa;
        self.zobacz = function (dane) {
            console.log(self.nazwa + " widzi:" + dane);
        };
    };


var obserwowany = new Obserwowany();
var obserwator1 = new Obserwator("Obserwator nr 1");
var obserwator2 = new Obserwator("Obserwator nr 2");
obserwowany.obserwuj(obserwator1.zobacz);
obserwowany.obserwuj(obserwator2.zobacz);
obserwowany.publikuj("dane nr 1");
obserwowany.nieobserwuj(obserwator2.zobacz);
obserwowany.publikuj("dane nr 2");
obserwowany.nieobserwuj(obserwator1.zobacz);
obserwowany.publikuj("dane nr 3");

//start obserwacji
//start obserwacji
//Obserwator nr 1 widzi:dane nr 1
//Obserwator nr 2 widzi:dane nr 1
//stop obserwacji
//Obserwator nr 1 widzi:dane nr 2
//stop obserwacji